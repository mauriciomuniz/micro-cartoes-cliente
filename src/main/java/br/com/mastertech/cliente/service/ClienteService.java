package br.com.mastertech.cliente.service;

import br.com.mastertech.cliente.model.Cliente;
import br.com.mastertech.cliente.respository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarClientePorID(int id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()) {
            Cliente cliente = clienteOptional.get();
            System.out.println("Cliente consultado: " + clienteOptional.get().toString() + " às " + LocalTime.now());
            return cliente;
        } else
            throw new RuntimeException("Cliente não encontrado");
    }
}
