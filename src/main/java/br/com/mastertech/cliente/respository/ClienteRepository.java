package br.com.mastertech.cliente.respository;

import br.com.mastertech.cliente.model.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
